package net.cjx.framework.widget.gifview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

/**
 * Created by appie on 13-12-31.
 */
public class GifView extends ImageView
{
    private GifFrame gifFrame;
    private final static String TAG = "cjx_GifView";

    public GifView(Context context)
    {
        super(context);
    }

    public GifView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public GifView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public void setGifImage(byte[] bytes)
    {
        if(gifFrame != null) gifFrame = null;
        gifFrame = GifFrame.CreateGifImage(bytes);
        if(gifFrame == null || gifFrame.size()<=0)
        {
            Log.e(TAG,"load gif image failure!,try to convert to bitmap");
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
            if(bitmap != null)
            {
                this.setImageBitmap(bitmap);
            }
            return;
        }

        AnimationDrawable drawable = new AnimationDrawable();
        for (int i=0;i<gifFrame.size();i++)
        {
            Bitmap bitmap = gifFrame.getFrameByIndex(i);
            if(bitmap != null)
            {
                BitmapDrawable drawable1 = new BitmapDrawable(getResources(),bitmap);
                drawable.addFrame(drawable1,500);
            }
        }
        setImageDrawable(drawable);
        drawable.start();


    }
}
