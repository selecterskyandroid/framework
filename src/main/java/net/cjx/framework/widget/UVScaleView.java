package net.cjx.framework.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

/**
 * 任意View缩放类，支持缩放、拖动、双击还原 使用方法
 * xml参考：
 <net.cjx.framework.widget.ViewScaleGesture
 android:layout_width="300dp"
 android:background="@android:color/white"
 android:layout_height="300dp">

 <View
 android:background="@drawable/page01_05"
 android:layout_width="match_parent"
 android:layout_height="match_parent"

 />

 </net.cjx.framework.widget.ViewScaleGesture>
 */
public class UVScaleView extends FrameLayout

{
    private static final String TAG = "cjx_UVScaleView";
    //确定划动的步长
    private final static int xStep = 20;
    //触摸类型
    private final static int TOUCH_UNKOWN = 0;
    private final static int TOUCH_SCALE = 1;
    private final static int TOUCH_MOVE = 2;
    private final static int TOUCH_DOUBLE_CLICK = 3;

    //最大缩放倍数
    private int maxZoom = 3;
    private float curZoom;

    private View mVSouce;
    private int touchType;

    //原始点
    int oldLeft,oldTop,oldRight,oldButtom;
    //上一次视图位置
    int lastLeft,lastTop,lastRight,lastButtom;
    //是否首次进入 保存原来尺寸
    private boolean isFirst;
    //划动开始点
    float beginX,beginY;
    //上一次弹起时间 用来检测双击
    long lastDownTime;
    //上一次缩放距离
    float lastSpan;

    private UVScaleListener listener;

    private void initData()
    {
        isFirst = true;
        touchType = TOUCH_UNKOWN;
        curZoom = 0;
    }

    public UVScaleView(Context context) {
        super(context);
        initData();
    }

    public UVScaleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initData();
    }

    public UVScaleView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initData();
    }

    public void setOnScaleListener(UVScaleListener listener_)
    {
        listener = listener_;
    }

    /**
     * 设置触摸缩放的子视图 如果没有设置，则默认为每一个子视图
     * @param view
     */
    public void setTouchChildView(View view)
    {
        mVSouce = view;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        //if(curZoom<=0)return false;
        int action = event.getAction();
        float lenX = 0.f,lenY = 0.f;
        int curWidth,oldWidth;
        if(mVSouce == null)
        {
            mVSouce = this.getChildAt(0);
        }
        if(mVSouce == null)
        {
            Log.e(TAG,"touch view is null");
            return  false;
        }


        switch (action & MotionEvent.ACTION_MASK)
        {
            case MotionEvent.ACTION_DOWN:
                if(isFirst)
                {
                    lastLeft = mVSouce.getLeft();
                    lastTop = mVSouce.getTop();
                    lastRight = mVSouce.getRight();
                    lastButtom = mVSouce.getBottom();

                    oldLeft = lastLeft;
                    oldTop = lastTop;
                    oldRight = lastRight;
                    oldButtom = lastButtom;

                    isFirst = false;
                }
                beginX = event.getX();
                beginY = event.getY();
                Log.d(TAG, "MotionEvent.ACTION_DOWN,beginX:" + beginX + ",beginY:" + beginY);
                break;
            //多个手指弹起
            case MotionEvent.ACTION_POINTER_UP:
                curWidth = mVSouce.getWidth();
                oldWidth = oldRight - oldLeft;
                //不允许缩小
                if(curWidth<oldWidth)
                {
                    resume();
                }
                break;
            //多个手指按下
            case MotionEvent.ACTION_POINTER_DOWN:
                touchType = TOUCH_SCALE;
                lastSpan = spacing(event);
                break;
            //
            case MotionEvent.ACTION_MOVE:

                if(touchType != TOUCH_SCALE)
                {
                    final float dx = event.getX();
                    final float dy = event.getY();

                    lenX = dx - beginX;
                    lenY = dy - beginY;

                    if((Math.abs(lenX)>xStep || Math.abs(lenY)>xStep) && curZoom>0)
                    {
                        touchType = TOUCH_MOVE;
                        move((int) (mVSouce.getLeft() + lenX), (int) (mVSouce.getTop() + lenY), (int) (mVSouce.getRight() + lenX), (int) (mVSouce.getBottom() + lenY));
                    }
                    beginX = dx;
                    beginY = dy;
                }
                else if(event.getPointerCount()>1)
                {
                    //当前两指间的距离
                    float j = spacing(event);

                    float x = (event.getX(1)+event.getX(0))/2;
                    float y = (event.getY(1)+event.getY(0))/2;

                    float zoom = j / lastSpan - 1;
                    int width = mVSouce.getWidth();
                    int height = mVSouce.getHeight();
                    float w0 = zoom * width;
                    float h0 = zoom * height;
                    float lSpan = (x-mVSouce.getLeft())*w0 / width;
                    float rSpan = (w0-lSpan);
                    float tSpan = (y - mVSouce.getTop())*h0 / height;
                    float bSpan = h0 - tSpan;

                    int l = (int) (mVSouce.getLeft()-lSpan);
                    int r = (int) (mVSouce.getRight() + rSpan);
                    int t = (int) (mVSouce.getTop() - tSpan);
                    int b = (int) (mVSouce.getBottom() + bSpan);

                    curWidth = r - l;
                    oldWidth = oldRight - oldLeft;

                    //判断最大的缩放倍数
                    if(oldWidth != 0 && (curWidth / oldWidth) <= maxZoom)
                    {
                        move(l, t, r, b);
                        curZoom = width / oldWidth;
                    }
                    lastSpan = j;
                    Log.d(TAG,"zoom:"+zoom+",w0:"+w0+",h0:"+h0+",l:"+l+",r:"+r+",t:"+t+",b:"+b);
                }

                break;
            case MotionEvent.ACTION_UP:

                //双击
                if(event.getEventTime() - lastDownTime < 200 && touchType == TOUCH_UNKOWN)
                {
                    onDoubleTap();
                    lastDownTime = 0;
                    return false;
                }
                touchType = TOUCH_UNKOWN;

                int span = 0;
                //右边超出了边界
                if(mVSouce.getLeft()>oldLeft)
                {
                    lastLeft = oldLeft;
                    span = mVSouce.getLeft() - oldLeft;
                    lastRight = mVSouce.getRight() - span;
                }
                else
                {
                    lastLeft = mVSouce.getLeft();
                }
                //左边超出边界
                if(mVSouce.getRight()<oldRight)
                {
                    lastRight = oldRight;
                    span = oldRight - mVSouce.getRight();
                    lastLeft = mVSouce.getLeft() + span;
                }
                else
                {
                    lastRight = mVSouce.getRight();
                }
                //下面超出边界
                if(mVSouce.getTop()>oldTop)
                {
                    lastTop = oldTop;
                    span = mVSouce.getTop()-oldTop;
                    lastButtom = mVSouce.getBottom() - span;
                }
                else
                {
                    lastTop = mVSouce.getTop();
                }
                //上面超出了边界
                if(mVSouce.getBottom()<oldButtom)
                {
                    lastButtom = oldButtom;
                    span = oldButtom - mVSouce.getBottom();
                    lastTop = mVSouce.getTop() + span;
                }
                else
                {
                    lastButtom = mVSouce.getBottom();
                }
                Log.d(TAG,"MotionEvent.ACTION_UP,lastLeft:"+lastLeft+",lastTop:"+lastTop+",lastRight:"+lastRight+",lastButtom:"+lastButtom);
                move(lastLeft,lastTop,lastRight,lastButtom);
                lastDownTime = event.getEventTime();
                if(listener != null)
                {
                    listener.onScaleFinish(UVScaleView.this);
                }
                break;
        }
        return true;
    }

    /**
     * 计算两指间的距离
     * @param event
     * @return
     */
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return FloatMath.sqrt(x * x + y * y);
    }

    //还原
    private void resume()
    {
        move(oldLeft, oldTop, oldRight, oldButtom);
        lastLeft = oldLeft;
        lastTop = oldTop;
        lastRight = oldRight;
        lastButtom = oldButtom;
        curZoom = 0;
    }

    /**
     * 改变视图
     * @param left
     * @param top
     * @param right
     * @param buttom
     */
    private void move(int left,int top,int right,int buttom)
    {

        int width = right - left;
        int height = top - buttom;
        int w = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
        int h = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY);
        mVSouce.measure(w, h);

        mVSouce.layout(left, top, right, buttom);


    }

    /**
     * 双击事件还原
     */
    public void onDoubleTap()
    {
        resume();
    }

    public interface UVScaleListener
    {
        public void onScaleFinish(UVScaleView view);
    }
}
