package net.cjx.framework.http;

import android.util.Log;

import net.tsz.afinal.FinalHttp;

import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

/**
 * Created by appie on 13-12-30.
 */
public class FinalHttpEx extends FinalHttp
{
    //返回类型
    public static final int RESPONSE_TYPE_JSON  = 1;
    public static final int RESPONSE_TYPE_TEXT = 2;
    public static final int RESPONSE_TYPE_IMAGE = 3;
    public static final int RESPONSE_TYPE_OTHER = 0;

    private static final String TAG = "cjx_FinalHttpEx";

    private static FinalHttpEx http;


    private String charset = "utf-8";
    //返回的格式
    private int response_type = RESPONSE_TYPE_OTHER;
    //返回状态码
    private int response_statusCode = 0;
    //设置请求的格式
    private int requset_type = RESPONSE_TYPE_JSON;

    public static FinalHttpEx create()
    {
        if(http == null)
        {
            http = new FinalHttpEx();
        }

        return http;
    }

    public static FinalHttpEx createSSL()
    {
        if(http == null)
        {
            http = new FinalHttpEx();
            http.setDefaultSSLfactory();
        }

        return http;
    }
    public String getCharset()
    {
        return charset;
    }

    public int getResponseType()
    {
        return response_type;
    }

    public int getResponseStatusCode()
    {
        return response_statusCode;
    }

    public int getRequsetType()
    {
        return requset_type;
    }

    public void setRequsetType(int requset_type)
    {
        this.requset_type = requset_type;
    }

    public FinalHttpEx()
    {
        configCharset(charset);

        DefaultHttpClient httpClient = (DefaultHttpClient)getHttpClient();
        httpClient.addRequestInterceptor(new HttpRequestInterceptor()
        {
            public void process(HttpRequest request, HttpContext context)
            {
                response_type = RESPONSE_TYPE_OTHER;
            }
       });

        httpClient.addResponseInterceptor(new HttpResponseInterceptor() {
            public void process(HttpResponse response, HttpContext context) {
                response_statusCode = response.getStatusLine().getStatusCode();
                Header header = response.getLastHeader("Content-Type");
                Log.d(TAG,"content-type name:"+header.getName()+",value:"+header.getValue());
                checkResponseType(header.getValue());
            }
        });
    }
    public boolean isRequestStringStream()
    {
        return (requset_type == RESPONSE_TYPE_JSON || requset_type == RESPONSE_TYPE_TEXT)?true:false;
    }
    private void setDefaultSSLfactory()
    {
        KeyStore trustStore = null;
        SSLSocketFactory sf = null;

        try
        {
            trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            sf = new SSLSocketFactoryEx(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        }catch (KeyStoreException e)
        {
            Log.d(TAG, "KeyStoreException");
            e.printStackTrace();
        }
        catch (CertificateException e)
        {
            Log.d(TAG,"CertificateException");
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e)
        {
            Log.d(TAG,"NoSuchAlgorithmException");
            e.printStackTrace();
        }
        catch (IOException e)
        {
            Log.d(TAG,"IOException");
            e.printStackTrace();
        }
        catch (KeyManagementException e)
        {
            Log.d(TAG,"KeyManagementException");
            e.printStackTrace();

        }
        catch (UnrecoverableKeyException e)
        {
            Log.d(TAG,"UnrecoverableKeyException");
            e.printStackTrace();
        }
        if(sf != null)
        {
            configSSLSocketFactory(sf);
        }
        else
        {
            Log.d(TAG,"SSLSocketFactory get failure");
        }
    }

    private void checkResponseType(String content_type)
    {
        Log.d(TAG,"content_type :"+content_type);
        if(content_type.indexOf("json")!=-1)
        {
            response_type = RESPONSE_TYPE_JSON;
        }
        else if(content_type.indexOf("text") != -1)
        {
            response_type = RESPONSE_TYPE_TEXT;
        }
        else if(content_type.indexOf("image") != -1)
        {
            response_type = RESPONSE_TYPE_IMAGE;
        }
        else
        {
            response_type = RESPONSE_TYPE_OTHER;
        }
    }
}
