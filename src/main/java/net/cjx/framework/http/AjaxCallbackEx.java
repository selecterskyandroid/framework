package net.cjx.framework.http;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import net.cjx.framework.utils.ActivityUtils;
import net.tsz.afinal.http.AjaxCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;

/**
 * Created by appie on 14-1-8.
 */
public class AjaxCallbackEx extends AjaxCallBack
{
    private static final String TAG = "cjx_AjaxCallbackEx";

    public Context mContext;

    //进度回调
    private ProgressCallback progressCallback;

    private FinalHttpEx http;
    //请求成功后后的回调函数
    private RequestCallback callback;

    public AjaxCallbackEx(Context context,FinalHttpEx http)
    {
        this(context,http,null);
    }
    public AjaxCallbackEx(Context context,FinalHttpEx http,RequestCallback cb)
    {
        mContext = context;
        setCallback(cb);
        this.http = http;
    }

    public void setCallback(RequestCallback callback)
    {
        this.callback = callback;
    }

    public void setProgressCallback(ProgressCallback progressCallback) {
        this.progressCallback = progressCallback;
    }

    /**
     * 进度回调，在加载前和加载后调用
     */
    public static interface ProgressCallback
    {
        /**
         * 是否要显示进度条
         * @param show true为显示进度条 false为不显示
         */
        public void onShowProgress(boolean show);
    }

    private void showProgressDialog(boolean show)
    {
        if(progressCallback != null)
        {
            progressCallback.onShowProgress(show);
        }

    }
    protected void triggerSuccess(int type,Object body)
    {
        if(callback != null)
        {
            callback.onSuccess(type,body);
        }
    }
    protected void triggerFailure(ResponseStatus status)
    {
        if(callback != null)
        {
            callback.onFailure(status);
        }
    }
    /**
     * 处理json数 如果想对json进行特殊判断，建议覆盖此方法
     * @param str_body
     */
    protected void processJsonData(String str_body)
    {
        //
        try
        {
            JSONObject json = new JSONObject(str_body);
            triggerSuccess(FinalHttpEx.RESPONSE_TYPE_JSON,json);
        } catch (JSONException e)
        {
            e.printStackTrace();
            triggerFailure(ResponseStatus.create(ResponseStatus.STATUS_STATUS_ERROR, "decode json failure:"+str_body));
        }
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Log.d(TAG,"onStart");
        showProgressDialog(true);
    }

    @Override
    public void onSuccess(Object o)
    {
        super.onSuccess(o);
        Log.d(TAG,"onSuccess");
        showProgressDialog(false);
        byte[] body = (byte[])o;
        if(http.isRequestStringStream())
        {
            String str_body = new String(body, Charset.forName(http.getCharset()));
            if(http.getRequsetType() == FinalHttpEx.RESPONSE_TYPE_JSON)
            {
                processJsonData(str_body);
                return;
            }
        }
        triggerSuccess(http.getRequsetType(),body);
    }

    @Override
    public void onFailure(Throwable t, int errorNo, String strMsg)
    {
        super.onFailure(t, errorNo, strMsg);
        Log.d(TAG,"onFailure:"+errorNo+",msg:"+strMsg);
        showProgressDialog(false);
        triggerFailure(ResponseStatus.create(errorNo, strMsg));
    }
}
