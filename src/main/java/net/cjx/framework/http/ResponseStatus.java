package net.cjx.framework.http;


/**
 * Created by appie on 14-1-2.
 */
public class ResponseStatus
{
    public static final int STATUS_START_POS = 1000;
    public static final int STATUS_DECODE_JSON_ERROR = STATUS_START_POS + 1;
    //服务器返回状态失败
    public static final int STATUS_STATUS_ERROR = STATUS_START_POS + 2;
    public static final int STATUS_DATA_GET_ERROR = STATUS_START_POS + 3;

    //0表示操作成功
    private int status;
    private String message;
    private Object flags;
    private static ResponseStatus instance;
    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public String getMessage()
    {
        return message;
    }

    public Object getFlags()
    {
        return flags;
    }

    public void setFlags(Object flags)
    {
        this.flags = flags;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
    public static ResponseStatus create(int status, String message)
    {
        if(instance == null)
        {
            instance = new ResponseStatus(status,message);
        }
        else
        {
            instance.setStatus(status);
            instance.setMessage(message);
        }
        return instance;
    }
    public ResponseStatus()
    {
        this(0,null);
    }
    public ResponseStatus(int status)
    {
        this(status,null);
    }

    public ResponseStatus(int status, String message)
    {
        this.status = status;
        this.message = message;
    }

    @Override
    public String toString()
    {
        return "ResponseStatus{" +
                "status=" + status +
                ", message='" + message + '\'' +
                '}';
    }
}
