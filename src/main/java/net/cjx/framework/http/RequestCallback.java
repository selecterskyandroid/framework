package net.cjx.framework.http;

/**
 * Created by appie on 14-1-2.
 */
public interface RequestCallback
{
    /**
     *
     * @param type 参考FinalHttpEx.RESPONSE_TYPE
     * @param body
     */
    public void onSuccess(int type,Object body);
    public void onFailure(ResponseStatus status);
}
