package net.cjx.framework.utils;

import android.util.Log;

import net.tsz.afinal.utils.FieldUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by appie on 14-1-3.
 */
public class JsonUtils
{
    private static final String TAG = "cjx_JsonUtils";

    /**
     * 通过json字符串，将数据解析到一个实体对象 要求实体对象定义的字段和json对象里面的字段对应，并且要设置get/set方法
     * @param body
     * @param tClass
     * @return 解析失败将返回null
     */
    public static Object decodeJsonObjectByString(String body,Class<?> tClass)
    {
        JSONObject json = null;
        try
        {
            json = new JSONObject(body);
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        if(json == null)
        {
            return null;
        }
        return decodeJsonObject(json,tClass);
    }

    /**
     * 通过Json对象，将数据解析到一个实体对象 要求实体对象定义的字段和json对象里面的字段对应，并且要设置get/set方法
     * @param json
     * @param tClass
     * @return
     */
    public static Object decodeJsonObject(JSONObject json,Class<?> tClass)
    {
        Object o = ObjectUtils.instanceByClass(tClass);
        if(o == null)return null;
        Field[]  fields = tClass.getDeclaredFields();
        Method method = null;
        //循环所有字段
        for (Field f:fields)
        {
            //Log.d(TAG,"name:"+f.getName()+",type:"+f.getType());
            method = FieldUtils.getFieldSetMethod(tClass,f);
            //没有设置方法 跳过
            if(method == null)continue;
            //json对象里面不存在此字段 跳过
            if(!json.has(f.getName()))continue;
            Object value = null;
            try
            {
                value = json.get(f.getName());
            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            //json对象里面的值为空 跳过
            if(value == null)continue;

            //获取list的数组对象的类型
            Class<?> type = f.getType();
            //内部对象 直接设置值即可
            if(ObjectUtils.isInternalType(type))
            {
                FieldUtils.setFieldValue(o,f,value);
            }
            //数组 递归解析数组
            else if(type.equals(List.class))
            {
                Type pt[] = method.getGenericParameterTypes();
                ParameterizedType para = (ParameterizedType)pt[0];
                Class arraytype = (Class)para.getActualTypeArguments()[0];
                Object arrayvalue  = decodeJsonArray((JSONArray)value,arraytype);
                FieldUtils.setFieldValue(o,f,arrayvalue);
            }
            //其它对象 必须是自定义的对象 递归解析
            else
            {
                Object objectvalue = decodeJsonObject((JSONObject) value, type);
                FieldUtils.setFieldValue(o,f,objectvalue);
            }
        }
        return o;
    }

    /**
     * 通过Json字符串数组对象，将数据解析到一个实体对象数组
     * @param json
     * @param tclass
     * @return
     */
    public static List<Object> decodeJsonArrayByString(String json,Class<?> tclass)
    {
        JSONArray jsonArray = null;
        try
        {
            jsonArray = new JSONArray(json);
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        if(jsonArray == null)return null;
        return decodeJsonArray(jsonArray,tclass);
    }

    /**
     * 通过Json数组对象，将数据解析到一个实体对象数组
     * @param json
     * @param tClass
     * @return
     */
    public static List<Object> decodeJsonArray(JSONArray json,Class<?> tClass)
    {
        List<Object> list = new ArrayList<Object>();
        Object value = null;
        //循环所有行
        for(int i=0;i<json.length();i++)
        {
            //获取值
            try
            {
                value = json.get(i);
            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            //值为空则跳过
            if(value == null)continue;
            //获取值类型
            Class<?> type = value.getClass();
            //如果是系统的类型，直接添加
            if(ObjectUtils.isInternalType(type))
            {
                list.add(value);
            }
            //如果是一个数组，则递归调用
            else if(type.equals(JSONArray.class))
            {
                value = decodeJsonArray((JSONArray) value, tClass);
                list.add(value);
            }
            //如果是一个对象，则调用对象解析 一般情况下是进入这个分支
            else if(type.equals(JSONObject.class))
            {
                value = decodeJsonObject((JSONObject)value,tClass);
                list.add(value);
            }
            else
            {
                Log.e(TAG, "unknown json row:" + i+",value:"+value);
            }
        }
        return list;
    }
}
