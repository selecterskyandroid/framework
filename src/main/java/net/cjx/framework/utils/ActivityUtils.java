package net.cjx.framework.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import net.cjx.framework.R;

/**
 * Created by appie on 13-12-31.
 */
public class ActivityUtils
{


    public static ProgressDialog progress(Context context,String msg)
    {
        return ProgressDialog.show(context,null,msg,false,false);
    }

    public static Toast toast(Context context,String msg)
    {
        Toast t = Toast.makeText(context,msg,Toast.LENGTH_SHORT);
        t.show();
        return t;
    }


    /**
     * 创建一个对话框
     * @param context
     * @param title
     * @param msg
     * @param yesListener
     * @param noListener
     * @return
     */
    public static Dialog dialog(Context context,String title,String msg,DialogInterface.OnClickListener yesListener, DialogInterface.OnClickListener noListener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(msg);
        if(yesListener != null)
        {
            builder.setPositiveButton(R.string.btn_sure, yesListener);
        }

        if(noListener != null)
        {
            builder.setNegativeButton(R.string.btn_cancel, noListener);
        }
        Dialog dialog = builder.create();
        return dialog;
    }

    /**
     * 创建一个输入对话框
     * @param context
     * @param title
     * @param yesListener
     * @param noListener
     * @return
     */
    public static Dialog inputDialog(Context context,String title, final View.OnClickListener yesListener, final View.OnClickListener noListener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        final EditText editText = new EditText(context);
        builder.setView(editText);
        builder.setPositiveButton(R.string.btn_sure, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                if(yesListener != null)
                {
                    yesListener.onClick(editText);
                }
            }
        });
        builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                if(noListener != null)
                {
                    noListener.onClick(editText);
                }
            }
        });
        return builder.create();
    }
}
